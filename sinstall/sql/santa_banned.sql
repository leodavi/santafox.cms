DROP TABLE IF EXISTS `%PREFIX%_banned`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_banned` ( `ip` bigint(15) NOT NULL,
 `forward_ip` bigint(15) DEFAULT NULL, 
`user_agent` varchar(255) DEFAULT NULL, 
`date_banned` datetime DEFAULT NULL, 
`date_unbanned` datetime DEFAULT NULL, 
`note` varchar(255) DEFAULT NULL, PRIMARY KEY (`ip`) ) 
ENGINE=MyISAM DEFAULT CHARSET=utf8 
COMMENT='Бан пользователей';